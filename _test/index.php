<html>
<head>
	<link rel="stylesheet" type="text/css" href="her.css">
	<link href="http://vjs.zencdn.net/4.1/video-js.css" rel="stylesheet">

	<script src="assets/scripts/js/libraries.js"></script>
	<script src="assets/scripts/js/bigvideo.js"></script>
</head>
<body>
	<div class='nav'>
		<button class='cache' data-type='all'>Home</button>
		<button class='cache' data-type='photo'>Photos</button>
		<button class='cache' data-type='video'>Videos</button>
		<button class='cache' data-type='gif'>GIFs</button>
		<button class='cache' data-type='next'>Next</button>
	</div>
	<div class='masthead'></div>
	<div class='container'></div>
	<div class='container_loading'></div>

	<script src="her.js"></script>
</body>
</html>