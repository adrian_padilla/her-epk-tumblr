/* Her Tumblr EPK Theme */
//$( document ).ready(function() {
	var environment = "dev",
		container = $('.container'),
		container_loading = $('.container_loading'),
		masthead = $('.masthead'),
		items = $('article'),
		page_type = "all",
		page_num = 0,
		base_URL = "http://rushthemovie.tumblr.com",
		url = {},
		path,
		cache = {},
		video_players = {},
		video_default = "video_home",
		posts_array,
		isotope_on;

	url.all = base_URL;
	url.photo = base_URL+"/tagged/photo";
	url.video = base_URL+"/tagged/video";
	url.gif = base_URL+"/tagged/gif";

	// load posts via filter
	function loadTumblrContent(type,callback) {
		
		page_num++;
		
		switch ( environment ) {
			case "dev" :
				path = "tumblrproxy.php?url="+encodeURIComponent(url[type])+"/page/"+page_num;
			break;
			case "production" :
				path = url[type]+"/page/"+page_num;
			break;
		}
		
		$.post(
			path,
			function(response) {
				posts_array = $(response).find('article'); 
				cache[type] = posts_array;
				
				if( typeof(callback) === 'function' )
					callback(posts_array);
			},
			"html"
		);
	}

	// isotope init
	function isotope_init() {
		container.isotope({
			itemSelector: 'article',
			layoutMode: 'masonry',
			transformsEnabled: false,
			animationEngine : "jquery",
			animationOptions: {
				duration: 400,
				queue: false,
				easing: 'easeOutExpo'
			},
			onLayout: function( $elems, instance ) {

			}
		});
		isotope_on = true;
	}

	$(function() {
	    var BV = new $.BigVideo({ "container": $('.masthead') });
	    BV.init();
	    BV.show('assets/videos/Her_Gif_03.mp4',{ambient:true});

	});

	// initial content batch
	loadTumblrContent( page_type, function(content) { 
		TweenMax.set( content, { opacity: 0 } );
		container.empty().append(content);
		container.imagesLoaded(function(){
			TweenMax.to( content, 1, { autoAlpha:1 } );
			isotope_init();
		});
	});
	

	// handle nav button clicks
	$('button').on('click',function() {
		
		switch ( true ) {
			case $(this).data('type') == "next" :
				page_type = page_type;
				console.log("next");
			break;
			
			default :
				page_type = $(this).data('type');
				page_num = 0;
				isotope_on = null;
			break;
		}
		
		loadTumblrContent( page_type, function(content) {
			TweenMax.set( content, { opacity: 0 } );
			switch ( true ) {
	    		case isotope_on :
	    			container.append(content);
	    			content.imagesLoaded(function(){
	    				TweenMax.to( content, 1, { autoAlpha:1 } );
	    				container.isotope( 'appended', content, true );
	    			});
	    		break;
				default :
	    			container_loading.empty().append(content);
	    			container_loading.imagesLoaded(function(){
	        			container.css('opacity', '0').empty().isotope( 'destroy' ).append(content).css('opacity', '1');
	        			TweenMax.to( content, 1, { autoAlpha:1 } );
	        			isotope_init();
					});
	    		break;
	    	}
	        	
	        switch ( true ) {
				case content.length < 10 :
					$('button[data-type=next]').hide();
				break;
				default :
					$('button[data-type=next]').show();
				break;
			} 
		});
	});


	// capture link tag clicks
	$('.container').on('click','.tags a', function() {
		
		url.tag = $(this).attr('href');
		page_type = "tag";
		page_num = 0;
		
		loadTumblrContent( page_type, function(content) { 
			TweenMax.set( content, { opacity: 0 } );
			container_loading.empty().append(content);
			container_loading.imagesLoaded(function(){
				container.css('opacity', '0').empty().isotope( 'destroy' ).append(content).css('opacity', '1');
				TweenMax.to( content, 1, { autoAlpha:1 } );
				isotope_init();
			});
		});

		return false;

	});

	/*
	$("#video_home").bind("loadedmetadata", function () {
	    w = this.videoWidth;
	    h = this.videoHeight;
	    aspect_h = parseFloat( h/w );
	    aspect_w = parseFloat( w/h );
	    resizePlayer();
	});
	*/

	// resize videos
	/*
	var video_el,
		video_orig_w,
	    video_orig_h,
	    aspect_w,
	    aspect_h;
	    */
	function resizePlayer(el) {
     	/*
	    if( el && el !== '' ) {
	    	video_el = $('#'+el);
	    	video_orig_w = video_el[0].videoWidth,
	    	video_orig_h = video_el[0].videoHeight,
	    	aspect_w = parseFloat(video_orig_w/video_orig_h),
	    	aspect_h = parseFloat(video_orig_h/video_orig_w);
	    }*/
	
	    //console.log('video_el: ',video_el);
	    //console.log('video_orig_w: ',video_orig_w);
	    //console.log('video_orig_h: ',video_orig_h);
	    
	    // scaling 
	        /*
	        var w_new,
	        	h_new,
	        	main_container = $('.masthead'),
	        	video = $('#video_home'),
	        	video_container = video.closest('.mejs-container');

	        
	        if( main_container.height() > ( aspect_w * video.height() ) ) {
	        	video.width( aspect_w * main_container.height() );
	        	video.height( main_container.height() );
	        	console.log('A');
	        }

	        if(main_container.width() > ( aspect_h * video.width() ) ) {
	        	video.width( main_container.width()  );
	        	video.height( aspect_h * main_container.width() );
	        	console.log('B');
	        }

	        return;
	        */

	   




	        
	        if ( $('#video_home').height() <= $('.masthead').height() ) {
	            w_new = 'auto';
	            h_new = $('.masthead').height();
	            //console.log('height A');
	        } else {
	            w_new = $('.masthead').width();
	            h_new = 'auto';
	            console.log('height B');
	        }
	        
	        if ( $('#video_home').width() <= $('.masthead').width() ) {
	            w_new = $('.masthead').width();
	            h_new = 'auto';
	            console.log('width A');
	        } else {
	            w_new = 'auto';
	            h_new = $('.masthead').height();
	            console.log('width B');
	        }
	    
	    // offset 
	        var Yoffset = $('#video_home').height()/2,
	            Xoffset = $('#video_home').width()/2,
	            YoffsetWindow =  $('.masthead').height()/2,
	            XoffsetWindow =  $('.masthead').width()/2,
	            top_new,
	            left_new;
	        
	        if( $('.masthead').height() <= $('#video_home').height() ){
	            top_new = -(Yoffset - YoffsetWindow);
	        }else{
	            top_new = 0;
	        }
	        
	        if( $('.masthead').width() <= $('#video_home').width() ){
	            left_new = -(Xoffset - XoffsetWindow);
	        }else{
	            left_new = 0;
			}
	    
	    $('#video_home').css({ "width": w_new, "height": h_new, "top": top_new, "left": left_new });

	    
	}


	// parallax your mind
	$(window).on('scroll',function() {
		masthead.css("transform", "translate3d(0px," + window.scrollY / 3 + "px, 0px)");
		container.css("transform", "translate3d(0px," + window.scrollY / 500 + "px, 0px)");
	});

	$(window).on('resize',function() {
	   // resizePlayer();
	});
//}); //document ready close