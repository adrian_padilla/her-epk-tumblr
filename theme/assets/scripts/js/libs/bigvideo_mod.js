/*
	BigVideo - The jQuery Plugin for Big Background Video (and Images)
	by John Polacek (@johnpolacek)
	
	Dual licensed under MIT and GPL.

    Dependencies: jQuery, jQuery UI (Slider), Video.js, ImagesLoaded
*/

;(function($) {

    $.BigVideo = function(options) {

        var defaults = {
			// If you want to use a single mp4 source, set as true
			useFlashForFirefox:true,
			// If you are doing a playlist, the video won't play the first time
			// on a touchscreen unless the play event is attached to a user click
			forceAutoplay:false,
			controls:true,
			doLoop:false,
			container: $('body'),
			video_element_id: null,
			video_element_class: '.big-video-vid',
			video_element_wrapper: 'big-video-wrap'
		},
		
		settings = $.extend({}, defaults, options),
		
		
		BigVideo = this,
			player,
			el_idx = 0,
			vidEl = settings.video_element_class,
			wrap = $('<div class="'+settings.video_element_wrapper+'"></div>'),
			mediaAspect = 16/9,
			vidDur = 0,
			defaultVolume = 0.8,
			isInitialized = false,
			isSeeking = false,
			isPlaying = false,
			isQueued = false,
			isAmbient = false,
			playlist = [],
			currMediaIndex,
			currMediaType = 'video';

        
        
        
        function updateSize() {
			var windowW = $(window).width();
			var windowH = $(window).height();
			var windowAspect = windowW/windowH;
			
			//console.log('el_idx:',el_idx);
			
			if (windowAspect < mediaAspect) {
				// taller
				if (currMediaType === 'video') {
					player
						.width(windowH*mediaAspect)
						.height(windowH);
					$('#'+el_idx,wrap)
						.css('top',0)
						.css('left',-(windowH*mediaAspect-windowW)/2)
						.css('height',windowH);
					$('#'+el_idx+'_html5_api',wrap).css('width',windowH*mediaAspect);
					$('#'+el_idx+'_flash_api',wrap)
						.css('width',windowH*mediaAspect)
						.css('height',windowH);
				} else {
					// is image
					$('.big-video-image',wrap)
						.css({
							width: 'auto',
							height: windowH,
							top:0,
							left:-(windowH*mediaAspect-windowW)/2
						});
				}
			} else {
				// wider
				if (currMediaType === 'video') {
					player
						.width(windowW)
						.height(windowW/mediaAspect);
					$('#'+el_idx,wrap)
						.css('top',-(windowW/mediaAspect-windowH)/2)
						.css('left',0)
						.css('height',windowW/mediaAspect);
					$('#'+el_idx+'_html5_api',wrap).css('width','100%');
					$('#'+el_idx+'_flash_api',wrap)
						.css('width',windowW)
						.css('height',windowW/mediaAspect);
				} else {
					// is image
					$('.big-video-image',wrap)
						.css({
							width: windowW,
							height: 'auto',
							top:-(windowW/mediaAspect-windowH)/2,
							left:0
						});
				}
			}
		}

		function initPlayControl() {
			// create video controller
			var markup = '<div class="big-video-control-container">';
			markup += '<div class="big-video-control">';
			markup += '<a href="#" class="big-video-control-play"></a>';
			markup += '<div class="big-video-control-middle">';
			markup += '<div class="big-video-control-bar">';
			markup += '<div class="big-video-control-bound-left"></div>';
			markup += '<div class="big-video-control-progress"></div>';
			markup += '<div class="big-video-control-track"></div>';
			markup += '<div class="big-video-control-bound-right"></div>';
			markup += '</div>';
			markup += '</div>';
			markup += '<div class="big-video-control-timer"></div>';
			markup += '</div>';
			markup += '</div>';
			settings.container.append(markup);

			// hide until playVideo
			$('.big-video-control-container',wrap).css('display','none');

			// add events
			$('.big-video-control-track',wrap).slider({
				animate: true,
				step: 0.01,
				slide: function(e,ui) {
					isSeeking = true;
					$('.big-video-control-progress',wrap).css('width',(ui.value-0.16)+'%');
					player.currentTime((ui.value/100)*player.duration());
				},
				stop:function(e,ui) {
					isSeeking = false;
					player.currentTime((ui.value/100)*player.duration());
				}
			});
			$('.big-video-control-bar',wrap).click(function(e) {
				player.currentTime((e.offsetX/$(this).width())*player.duration());
			});
			$('.big-video-control-play',wrap).click(function(e) {
				e.preventDefault();
				playControl('toggle');
			});
			player.on('timeupdate', function() {
				if (!isSeeking && (player.currentTime()/player.duration())) {
					var currTime = player.currentTime();
					var minutes = Math.floor(currTime/60);
					var seconds = Math.floor(currTime) - (60*minutes);
					if (seconds < 10) seconds='0'+seconds;
					var progress = player.currentTime()/player.duration()*100;
					$('.big-video-control-track',wrap).slider('value',progress);
					$('.big-video-control-progress',wrap).css('width',(progress-0.16)+'%');
					$('.big-video-control-timer',wrap).text(minutes+':'+seconds+'/'+vidDur);
				}
			});
		}

		function playControl(a) {
			var action = a || 'toggle';
			if (action === 'toggle') action = isPlaying ? 'pause' : 'play';
			if (action === 'pause') {
				player.pause();
				$('.big-video-control-play',wrap).css('background-position','-16px');
				isPlaying = false;

			} else if (action === 'play') {
				player.play();
				$('.big-video-control-play',wrap).css('background-position','0');
				isPlaying = true;
			}
		}

		function setUpAutoPlay() {
			player.play();
			settings.container.off('click',setUpAutoPlay);
        }

		function nextMedia() {
			currMediaIndex++;
			if (currMediaIndex === playlist.length) currMediaIndex=0;
			playVideo(playlist[currMediaIndex]);
        }

        function playVideo(source) {

			// clear image
			$(vidEl,wrap).css('display','block');
			currMediaType = 'video';
			player.src(source);
			isPlaying = true;
			if (isAmbient) {
				$('.big-video-control-container',wrap).css('display','none');
				player.ready(function(){
					player.volume(0);
				});
				doLoop = true;
			} else {
				$('.big-video-control-container',wrap).css('display','block');
				player.ready(function(){
					player.volume(defaultVolume);
				});
				doLoop = false;
			}
			$('.big-video-image',wrap).css('display','none');
			$(vidEl).css('display','block');
        }

        function showPoster(source) {
			// remove old image
			$('.big-video-image',wrap).remove();

			// hide video
			player.pause();
			$(vidEl,wrap).css('display','none');
			$('.big-video-control-container',wrap).css('display','none');

			// show image
			currMediaType = 'image';
			var bgImage = $('<img class="big-video-image" src='+source+' />');
			wrap.append(bgImage);

			$('.big-video-image',wrap).imagesLoaded(function() {
				mediaAspect = $('.big-video-image',wrap).width() / $('.big-video-image',wrap).height();
				updateSize();
			});
        }

		BigVideo.init = function() {
			
			var c = parseInt( $('.video-js').length ) + 1 || 0;
			el_idx = settings.video_element_id ? settings.video_element_id :  vidEl.substr(1)+'_'+c;
			
			if (!isInitialized) {
				// create player
				settings.container.prepend(wrap);
				var autoPlayString = settings.forceAutoplay ? 'autoplay' : '';
				player = $('<video id="'+el_idx+'" class="video-js vjs-default-skin" preload="auto" data-setup="{}" '+autoPlayString+' webkit-playsinline></video>');
				player.css('position','absolute');
				wrap.append(player);

				var videoTechOrder = ['html5','flash'];
				// If only using mp4s and on firefox, use flash fallback
				var ua = navigator.userAgent.toLowerCase();
				var isFirefox = ua.indexOf('firefox') != -1;
				if (settings.useFlashForFirefox && (isFirefox)) {
					videoTechOrder = ['flash', 'html5'];
				}
				player = videojs(el_idx, { 
					controls:false, 
					autoplay:true, 
					preload:'auto', 
					techOrder:videoTechOrder
				});

				// add controls
				if (settings.controls) initPlayControl();

				// set initial state
				updateSize();
				isInitialized = true;
				isPlaying = false;

				if (settings.forceAutoplay) {
					$('body').on('click', setUpAutoPlay);
				}

				$('#'+el_idx+'_flash_api',wrap)
					.attr('scale','noborder')
					.attr('width','100%')
					.attr('height','100%');
					
				// set events
				$(window).resize(function() {
					updateSize();
				});

				player.on('loadedmetadata', function(data) {
					if (document.getElementByClass && document.getElementByClass('#'+el_idx+'_flash_api')) {
						// use flash callback to get mediaAspect ratio
						mediaAspect = document.getElementByClass('#'+el_idx+'_flash_api').vjs_getProperty('videoWidth')/document.getElementByClass('#'+el_idx+'_flash_api').vjs_getProperty('videoHeight');
					} else {
						// use html5 player to get mediaAspect
						mediaAspect = $('#'+el_idx+'_html5_api',wrap).prop('videoWidth')/$('#'+el_idx+'_html5_api',wrap).prop('videoHeight');
					}
					updateSize();
					var dur = Math.round(player.duration());
					var durMinutes = Math.floor(dur/60);
					var durSeconds = dur - durMinutes*60;
					if (durSeconds < 10) durSeconds='0'+durSeconds;
					vidDur = durMinutes+':'+durSeconds;
				});

				player.on('ended', function() {
					if (settings.doLoop) {
						player.currentTime(0);
						player.play();
					}
					if (isQueued) {
						nextMedia();
					}
				});
			}
        };

        BigVideo.show = function(source,options) {
			if (options === undefined) options = {};
			isAmbient = options.ambient === true;
			if (isAmbient || options.doLoop) settings.doLoop = true;
			if (typeof(source) === 'string') {
				var ext = source.substring(source.lastIndexOf('.')+1);
				if (ext === 'jpg' || ext === 'gif' || ext === 'png') {
					showPoster(source);
				} else {
					if (options.altSource && navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
						source = options.altSource;
					}
					playVideo(source);
					isQueued = false;
				}
			} else {
				playlist = source;
				currMediaIndex = 0;
				playVideo(playlist[currMediaIndex]);
				isQueued = true;
			}
        };

        // Expose Video.js player
        BigVideo.getPlayer = function() {
			return player;
        };

        // Expose BigVideoJS player actions (like 'play', 'pause' and so on)
        BigVideo.triggerPlayer = function(action){
			playControl(action);
        };
    };

})(jQuery);