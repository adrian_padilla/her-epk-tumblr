/* Her Tumblr EPK Theme */

// variables
	//var scrollTop = document.documentElement ? document.documentElement.scrollTop : document.body.scrollTop ? document.body.scrollTop : window.pageYOffset;
	var ie8,
		safari,
		url_hash_init = true,
		first_load = true,
		//header = $('header'),
		container_wrapper = $('.container_wrapper'),
		container = $('.container', container_wrapper),
		container_loading = $('.container_loading'),
		masthead = $('.masthead'),
		modal_features = $('.modal_features', masthead),
		section_current = $('section.on', masthead),
		//tt_lockup = $('.tt_lockup', masthead),
		video_wrapper,
		//video_active,
		videos_array = [],
		//videos_home = [],
		//videos_other = [],
		//video_count,
		video_idx = 0,
		BV1_changed = true,
		youtube_src = "//www.youtube.com/embed/ne6p6MfLBxc?rel=0enablejsapi=1&html5=1",
		//tumblrAPIKey = "F8sWo7u1ZD52x74sOyunHr7C0YIYAHwD2n2YMdFRv5jiZhGQk7",
		loader = $('.loader'),
		loader_text = $('.loader .text'),
		loadmore_text = $('.moreposts span'),
		page_type = "home",
		url = {},
		path,
		sticky_path,
		permalink,
		tagged,
		_isotope = false,
		//cache = {},
		BV1,
		BV2,
		all_posts,
		posts_obj,
		sticky_obj,
		stickies = false,
		calls_complete = 0,
		activity_complete=0,
		animate = false,
		loading = true,
		more = true,
		settings = {"easing": "Expo.easeOut",
					"eventType": { "scroll": "scroll", "click": "click"}
					},
		dynamic_date = $(".dynamic_date"),
		//YT_Players = [],
		topOffSet = 10,
		hOffSet = 200;

	

	if ( Modernizr.touch ) {
		settings.eventType.scroll = "touchend";
		settings.eventType.click = "touchstart";
		topOffSet = 10;
		animateTTout();
		//console.log("hi");
	}
	
//	
	url.home = base_URL;
	url.video = base_URL+"/tagged/video";
	url.about = base_URL+"/tagged/about";
	url.gallery = base_URL+"/tagged/gallery";
	url.downloads = base_URL+"/tagged/downloads";
	url["everything-about-everything"] = base_URL+"/tagged/everything-about-everything";
	url.soundtrack = base_URL+"/tagged/soundtrack";
	
	url.all = base_URL;
	url.gif = base_URL+"/tagged/gif";
	url.image = base_URL+"/tagged/image";
	url.text = base_URL+"/tagged/text";

	
	
	if ($.browser.msie && parseInt($.browser.version, 10) === 8) ie8 = true;

	if ($.browser.webkit && !(/chrome/.test(navigator.userAgent.toLowerCase()))) safari = true;



/******
Preload / Initialize / Set up 
******/




// date messaging
	var curr_date = new Date(),
		next_week = new Date("December 16, 2013"),
		this_week = new Date("December 17, 2013"),
		now_playing = new Date("December 18, 2013");

	switch (true){

		case curr_date >= now_playing :
			dynamic_date.html( dynamic_date.data('nowplaying') );
			break;

		case curr_date >= this_week :
			dynamic_date.html(dynamic_date.data('thisweek'));
			break;

		case curr_date >= next_week :
			dynamic_date.html(dynamic_date.data('nextweek'));
			break;

	}
	
	dynamic_date.each(function() {
		if( $(this).parent().hasClass('media_filter') ) {
			var c = $(this).html();
			if(c.match('<br>')) {
				var a = c.split('<br>');
				$(this).html(a[0]);
			}
		}
	});

// isotope centering
	$.Isotope.prototype._getCenteredMasonryColumns = function() {

		this.width = this.element.width();

		var parentWidth = this.element.parent().width();

		var colW = this.options.masonry && this.options.masonry.columnWidth || // i.e. options.masonry && options.masonry.columnWidth

		this.$filteredAtoms.outerWidth(true) || // or use the size of the first item

		parentWidth; // if there's no items, use size of container

		var cols = Math.floor(parentWidth / colW);

		cols = Math.max(cols, 1);

		this.masonry.cols = cols; // i.e. this.masonry.cols = ....
		this.masonry.columnWidth = colW; // i.e. this.masonry.columnWidth = ...
	};

	$.Isotope.prototype._masonryReset = function() {

		this.masonry = {}; // layout-specific props
		this._getCenteredMasonryColumns(); // FIXME shouldn't have to call this again

		var i = this.masonry.cols;

		this.masonry.colYs = [];
			while (i--) {
			this.masonry.colYs.push(0);
		}
	};

	$.Isotope.prototype._masonryResizeChanged = function() {

		var prevColCount = this.masonry.cols;

		this._getCenteredMasonryColumns(); // get updated colCount
		return (this.masonry.cols !== prevColCount);
	};

	$.Isotope.prototype._masonryGetContainerSize = function() {

		var unusedCols = 0,

		i = this.masonry.cols;
			while (--i) { // count unused columns
			if (this.masonry.colYs[i] !== 0) {
				break;
			}
			unusedCols++;
		}

		return {
			height: Math.max.apply(Math, this.masonry.colYs),
			width: (this.masonry.cols - unusedCols) * this.masonry.columnWidth // fit container to columns that have been used;
		};
	};

// isotope init
	function isotopeInit() {
		//console.log('isotopeInit()');
		TweenMax.to( container, 0.2, { autoAlpha:0 } );
		photosetInit( function(){
			container.isotope({
				itemSelector: '.post',
				layoutMode: 'masonry',
				masonry: {
					columnWidth: 1
				},
				transformsEnabled: false,
				animationEngine : "jquery",
				animationOptions: {
					duration: 400,
					queue: false,
					easing: 'easeOutExpo'
				},
				onLayout: function( $elems, instance ) {
					fitvid_init();
					TweenMax.to( container, 1, { autoAlpha:1 } );
				}
			});

			_isotope = true;

		});

		
		loading = false;
		
	}

// photoset init
	function photosetInit(onComplete) {
		
		if( $('.photoset-grid').length === 0 ) {
			if( typeof(onComplete) === "function")
				onComplete();
			return;
		}

		$('.photoset-grid').photosetGrid({
			highresLinks: true,
			rel: $('.photoset-grid').attr('data-id'),
			gutter: '5px',

			onComplete: function(){
				$(".photoset-grid").attr("style", "");
				$(".photoset-grid a").colorbox({
					photo: true,
					scalePhotos: false,
					maxHeight: "90%",
					maxWidth: "90%",
					transition: "none",
					title: function () {
					    return $("img", this).attr("alt")
					}
				});
				$(".photoset-grid").each(function () {
					var e = $(this).attr("data-id");
					$(this).find(".photoset-cell").attr("rel", e)
				});

				if ( typeof(onComplete) === "function")
					onComplete();

			}
			
		});
	}

// fitvid init
	function fitvid_init(){
		$(".post.vid .post_content").fitVids();
	}

// custom scroll bar init
	function custom_scrollbar(){
		$(".masthead section.about .container .content.synopsis").mCustomScrollbar();
	}

// bigvideo init
	$(function() {
		
		switch ( true ){
			case Modernizr.touch:
				// do nothing
				//BV1 = new $.BigVideo({ "container": $('.masthead'), useFlashForFirefox: false });
				//BV1.init();
				//BV1.show(videos_home[1]+'.jpg',{ ambient:true});
				
			break;
			default:
				BV1 = new $.BigVideo({ "container": $('.masthead'), useFlashForFirefox: false });
				BV2 = new $.BigVideo({ 'container': $('.masthead'), useFlashForFirefox: false });
				BV1.init();
				BV2.init();

				switch ( true ){
					case ie8:
						BV1.show(videos_home[0]+'.jpg',{ ambient:true});
						BV2.show(videos_home[1]+'.jpg',{ ambient:true});
					break;
					default:
						BV1.show(videos_home[0]+'.mp4',{ altSource:videos_home[0]+'.ogv', ambient:true});
						BV2.show(videos_home[1]+'.mp4',{ altSource:videos_home[0]+'.ogv', ambient:true});
				}
		}

		video_wrapper = $('.big-video-wrap',masthead);
		video_wrapper.first().addClass('inactive');
		
		if ( safari )
			video_wrapper.addClass('ios');
	});



/******
Systemic
******/

// append new posts
	function appendNewPosts(){
		loading = true;
		
		stickies = true;

		loadmore_text.html(loadmore_text.data('loading'));

		loadTumblrContent( page_type, function(content) {
			TweenMax.set( content, { opacity: 0 } );
			container.append(content);
			
			container.imagesLoaded(function(){
				
				YThtml5(content);
				fitvid_init();

				photosetInit(function(){
					container.isotope( 'appended', $(content), function(){
						container.isotope( 'reLayout', true );
						TweenMax.to( $(content), 1, { autoAlpha:1 } );
					} );
				});
				
				parseAuthors();
				parseFeatured();
				removeVialinks();

				

				loading = false;
				countPosts(content);
			});
			
		});
	}

// Add HTML5 YouTube
	function YThtml5(content){
		if ( !ie8 && !ie10 && !safari ){
			$(content).filter('.vid').each(function() { 
			    var el = $('.post_content iframe',$(this));
			    console.log("YThtml5 element ",el);
			    try{
				    if( el.attr('src').indexOf('youtu') >= -1 ){
				    	el.attr("allowfullscreen", 1);
				    	el.attr('src',el.attr('src').replace('?','?html5=1&enablejsapi=1&'));
				    }
				 }catch(err){}
			});
			////console.log("YThtml5(ie10) = "+ie10);
		}
	}
	
// Laurels Rotator
	
	var mod_features = (function($) {
		console.log("mod_features INIT")
		var container,
			item_count,
			mask,
			slider,
			slider_items,
			slider_item_current,
			slider_item_next,
			idx;


		function init( el ) {
			
			container = typeof(el) === 'string' ? $(el) : el;
			mask = $('.mask',container);
			slider = $('ul.slider',mask);
			slider_items = $('li',slider);
			slider_item_current = $('li.on',slider);
			item_count = parseInt(slider_items.length,10);
			idx = 0;

			reset();

		}
		
		function reset() {
			console.log("RESET");
			TweenMax.killDelayedCallsTo(next);
			TweenMax.killAll(false, false, true);

			slider_items.removeClass('on');
			slider_items.first().addClass('on');

			next();
		}

		function next() {

			idx++;
			if( idx > item_count-1 )
				idx = 0;

			TweenMax.killDelayedCallsTo(next);
			slide();
		}

		function prev() {
			idx--;
			if( idx < 0 )
				idx = item_count-1;

			slide();
		}

		function slide() {
			slider_item_next = slider_items[idx];


			//TweenMax.set( slider_items, { className: '-=on' });
			//TweenMax.set( slider_item_next, { className: '+=on', autoAlpha: 0 });
			//TweenMax.to( slider_item_next, 0.8, { autoAlpha: 1 });

			var tl = new TimelineLite();
			tl.to( slider_items, 0.8, { autoAlpha: 0, delay: 5  });
			tl.set( slider_items, { className: '-=on' });
			tl.set( slider_item_next, { className: '+=on', autoAlpha: 0 });
			tl.to( slider_item_next, 0.8, { autoAlpha: 1 });



			TweenMax.delayedCall(7, next);

		}

		return {
			'init': init,
			'reset': reset,
			'next': next,
			'prev': prev
		};
	}(jQuery));
	

	/*
	$('nav a',modal_features).on(settings.eventType.click, function() {
		if( $(this).hasClass('left') )
			mod_features.prev();
		else
			mod_features.next();
	});
	*/

// format soundcloud posts
	/*
	function formatSoundcloudPosts() {
		
		if( soundcloud.length > 0 ) {
			for(var x in soundcloud ) {
				var p = soundcloud[x];
				$('article[data-post-id='+p.id+']').removeClass('vid');
			}

		}
	}*/

// slider
	$('body').on(settings.eventType.click,'.slider-btn', function() {
		var expandable = $('.expandable', $(this).parent());
		
		switch( true ) {
			case expandable.hasClass('open'):
				TweenMax.to(expandable,0.2,{ className: "-=open" });
				TweenMax.to($('.slider',expandable),0.2,{ className: "-=open" });
			break;
			default:
				TweenMax.to(expandable,0.2,{ className: "+=open" });
				TweenMax.to($('.slider',expandable),0.2,{ className: "+=open" });
			break;
		}
	});

// Add Authors
	
	function parseAuthors() {
		//console.log('parseAuthors()');
		var markup,matches,post_id,tag,tagsearch,authors_array = [];
		
		$('.post').each(function(){ 
			markup = $(this).html();
			post_id = $(this).data('post-id');

			tagsearch = markup.match(/_sTag_(.+)_eTag_/);
			////console.log('tagsearch',tagsearch);

			if(tagsearch && tagsearch.length > 1 ) {
				tags = tagsearch[1].split(',');
				////console.log('tags',tags);
				for(var x in tags ) {
					////console.log('tags['+x+']',tags[x]);
					if( tags[x].toLowerCase().match(/author\:/i) ) {
						authors_array.push({ "author": tags[x].replace(/Author\:/i,''), "id": post_id });
					}
				}
			}
		});

		////console.log(authors_array);
		var obj;
		for(var x in authors_array) {
			obj = authors_array[x],
			el = $('#p'+obj.id+'_author');
			el.addClass('on');
			el.html("Author: "+obj.author);
		}

		authors_array = [];
	}

// Add .featured
	function parseFeatured() {
		//console.log('parseFeatured()');
		var markup,matches,post_id,tag,tagsearch,featured_array = [];
		
		$('.post').each(function(){ 
			markup = $(this).html();
			post_id = $(this).data('post-id');

			tagsearch = markup.match(/_sTag_(.+)_eTag_/);
			////console.log('tagsearch',tagsearch);

			if(tagsearch && tagsearch.length > 1 ) {
				tags = tagsearch[1].split(',');
				////console.log('tags',tags);
				for(var x in tags ) {
					////console.log('tags['+x+']',tags[x]);
					if( tags[x].toLowerCase().match("featured") ) {
						featured_array.push({ "id": post_id });
					}
				}
			}
		});

		//console.log("featured_array",featured_array);
		var obj;
		for(var x in featured_array) {
			obj = featured_array[x],
			el = $('#p'+obj.id);
			el.addClass('featured');
			//el.html("Author: "+obj.author);
		}

		featured_array = [];
	}

// Create Source Link

	function removeVialinks(){
		//console.log('removeVialinks()');
		$('.post .caption > p').each(function(){
			var markup = $(this).html();
			if ( markup && markup.match("via") )
				$(this).html('');
		});
		
		
	}


/******
Sequence
******/

// load posts via AJAX 
	function loadTumblrContent(type,callback) {
				
	// Preppend proxy is needed
	// and asign page number
		page_num++;
		calls_complete = 0;
		//console.log("LOADING FUCKING TUMBLR");
		//console.log("page_num",page_num);
		//console.log("page_type",type);
		switch ( environment ) {
			case "dev" :
				switch ( true ) {
					case permalink :
						path = "tumblrproxy.php?url="+encodeURIComponent(url[type]);
						//sticky_path = "";
					break;
					default :
						path = "tumblrproxy.php?url="+encodeURIComponent(url[type])+"/page/"+page_num;
						sticky_path = "tumblrproxy.php?url="+base_URL+"/tagged/pinned";
				}
				
			break;
			case "production" :
				switch ( true ) {
					case permalink :
						path = url[type];
						sticky_path = "";
					break;
					default :
						path = url[type]+"/page/"+page_num;
						sticky_path = base_URL+"/tagged/pinned";
				}
			break;
		}
		
	// AJAX 
	
	// Gets post for assigned page number
		$.post(
			path,
			function(response) {
				posts_obj = $(response).find('.post');
				
				if ( type == "home" && stickies == false ){
					//console.log("AJAX posts_obj");
					$(document).trigger('AJAX-DONE');
				}else{
					if( typeof(callback) === 'function' ){
						//console.log("SKIPPED STICKIES");
						callback(posts_obj);	
					}
				}
				

			},
			"html"
		);

	// Gets sticky posts
		
		if ( type == "home" && !stickies ){
			$.post(
				sticky_path,
				function(response) {
					sticky_obj = $(response).find('.post');
					
					//console.log("AJAX sticky_obj");
					$(document).trigger('AJAX-DONE');
				},
				"html"
			).fail(function(){
				sticky_obj = {};
				//console.log("STICKY FAIL");
				$(document).trigger('AJAX-DONE');
			});
		}
			

	// Combine arrays
		$(document).on('AJAX-DONE',function() {
			//console.log("CALLS as arrived",calls_complete);
			
			calls_complete++;
			
			//console.log("CALLS after ++",calls_complete);

			if( calls_complete == 2 ) {
				console.log("AJAX ALL DONE");
				
				calls_complete = 0;
				
				var post_keys = {};
    			
    			all_posts = "";

    			sticky_obj.each(function() {
					var that = $(this);

					post_keys[that.data('post-id')] = true;

					all_posts += that[0].outerHTML;
					stickies = true;
				});

				posts_obj.each(function() {
					var that = $(this);
					if(post_keys[that.data('post-id')])
						return;

					post_keys[that.data('post-id')] = true;

					all_posts += that[0].outerHTML;
				});

				
				if( typeof(callback) === 'function' )
					callback(all_posts);
				
			}
		});
		
		

	}

// masthead rotate
	function mastheadRotate(page, parent){
		console.log("MASTHEAD!!");
		switch ( true ) {
			case parent.hasClass('media_filter'):
				animate = false;
				getPosts(page_type);
				return false;
			break;
			default:
				animate = true;
			break;
		}

		//console.log('mastheadRotate(animate: '+animate+')');
		
		$('body').animate({scrollTop:0}, 300, function(){
		
			if ( animate ) {
				
				var tl = new TimelineLite({ onComplete: rotateVideos, onCompleteParams:[page] });
				tl.to( $('h1', section_current), 0.5, { className: '+=closed_left', ease:settings.easing });
				tl.to( $('.content', section_current), 0.5, { className: '+=closed_left', ease:settings.easing }, "-=0.3");
				tl.to( $('p', section_current), 0.5, { className: '+=closed_left', ease:settings.easing }, "-=0.3");
				tl.set( section_current, { className: '-=on' });
				tl.set( $('h1', section_current), { className: '-=closed_left' });
				tl.set( $('.content', section_current), { className: '-=closed_left' });
				tl.set( $('p', section_current), { className: '-=closed_left' });
			}

			//swapVideos(page);
			
		});
	}

// swap out videos
 function swapVideos(page){

 	console.log('swapVideos('+page+')');

 	switch ( true ){
 		case Modernizr.touch:
 			return false;
 			break;

 		default:

	 		videos_array = [];
			
			switch( true ) {
				case page === 'home':
					videos_array = videos_home;
					break;
				default:
					videos_array = videos_array.concat(videos_home,videos_other);
			}

			video_idx++;
			
			if ( video_idx >= videos_array.length )
				video_idx = 0;

			switch ( true ) {
				case BV1_changed:
					switch ( true ){
						case ie8:
							BV2.show(videos_array[video_idx]+'.jpg',{ ambient:true});
						break;
						default:
							BV2.show(videos_array[video_idx]+'.mp4',{ altSource:videos_array[video_idx]+'.ogv', ambient:true});
					}

					BV1_changed = false;
					break;
				
				default:
					switch ( true ){
						case ie8:
							BV1.show(videos_array[video_idx]+'.jpg',{ ambient:true});
						break;
						default:
							BV1.show(videos_array[video_idx]+'.mp4',{ altSource:videos_array[video_idx]+'.ogv', ambient:true});
					}

					BV1_changed = true;
					break;
			}
 	}

 }

// rotate videos
	function rotateVideos(page){
		console.log("ROTATE VIDEOS");
		//console.log('rotateVideos(page: '+page+')');
		// Video page specifics for loading / killing the player in the mastehad
		$('section.video .container .content').html('');
		if ( page_type == "video" )
			$('section.video .container .content').html('<iframe width="350" height="100%" scrolling="no" frameborder="no" src="'+youtube_src+'" allowfullscreen></iframe>');

		// Home page specifics for loading / killing the player in the mastehad
		//$('section.home .container .content.vid').html('');
		//if ( page_type == "home" )
		//	$('section.home .container .content.vid').html('<iframe width="350" height="100%" scrolling="no" frameborder="no" src="'+youtube_src+'" allowfullscreen></iframe>');

		if ( page_type == "everything-about-everything" )
			$('footer').css('display','none');
		else
			$('footer').css('display','block');

		switch ( true ){
			case Modernizr.touch:
				nextPage(page);
				break;
			default:
				/** override this on the first pass so we arrive at the correct 0 index **/
				if(first_load) {
					video_idx=-1;
					first_load = false;
				}
				swapVideos(page);
				var video_current = $('.big-video-wrap').not('.inactive'),
					video_new = $('.big-video-wrap.inactive');
				
				TweenMax.to( video_current, 0.5, { className: '+=transitioning', ease:settings.easing, onComplete: function() {
					TweenMax.set( video_new, { className: '-=inactive' });
					TweenMax.set( video_current, { className: '-=transitioning' });
					TweenMax.set( video_current, { className: '+=inactive' });

					nextPage(page);

				}});
		}
	};

// next page
	function nextPage(page){
		//console.log("NEXT PAGE");
		getPosts(page_type);
		
		section_current = $('.'+page);

		//console.log('nextPage section_current: '+page);
		//if ( page == "home" )
		

		TweenMax.set( $('h1', section_current), { className: '+=closed_right' });
		TweenMax.set( $('.content', section_current), { className: '+=closed_right' });
		TweenMax.set( $('p', section_current), { className: '+=closed_right' });
		TweenMax.set( section_current, { className: '+=on' });

		var tl = new TimelineLite({ onComplete: check_page });
		tl.to( $('h1', section_current), 0.5, { className: '-=closed_right', ease:settings.easing, delay:0.2 });
		tl.to( $('.content', section_current), 0.5, { className: '-=closed_right', ease:settings.easing }, "-=0.3");
		tl.to( $('p', section_current), 0.5, { className: '-=closed_right', ease:settings.easing }, "-=0.3");

		function check_page(){
			//mod_features.init(".modal_features");
			if (page_type == "home"){
				mod_features.reset();
				console.log("Reset from NEXTPAGE")
			}
			/**
			*	ie8 hack to remove inline-style from DOM
			*/
			$('h1', section_current).removeAttr("style");
			$('.content', section_current).removeAttr("style");
			$('p', section_current).removeAttr("style");

			$(document).trigger('MH-ANIM-DONE');
		}
	};

// Get Posts
	function getPosts(page_type){
		//console.log("GET POSTS!!!",page_type);
		stickies = false;
		//console.log("STICKIES",stickies);
		if ( animate ) {
			container_wrapper.addClass('closed');
			container.addClass('closed').empty();
		} else {
			if ( !tagged )
				$('body,html').animate({scrollTop: (masthead.height() - $('header').height() - topOffSet)});
			container.empty();
		}
	

		switch ( true ){
			case permalink:
				$('html').addClass('detail');
			break;
			case tagged:
				$('html').addClass('special');
			break;
			default:
				$('html').removeClass('detail');
				$('html').removeClass('special');
				$('.fb-comments').removeClass('on');

				if ( !Modernizr.touch )
					TweenMax.to( $('.masthead'), 0.2, { className: '-=closed', ease:Circ.easeOut });
		}

		if( _isotope )
			container.isotope('destroy');
		

		loadTumblrContent( page_type, function(content) {
			console.log("TUMBLR LOAD CALLBACK");
			$(document).unbind('AJAX-DONE');
			TweenMax.set( content, { opacity: 0 } );
			container_loading.empty().append(content);
			countPosts(content);

			container_loading.imagesLoaded(function(){
				container_wrapper.removeClass('closed');
				container.append(content).removeClass('closed');

				YThtml5(content);
				
				parseAuthors();
				parseFeatured();
				removeVialinks();
				

				TweenMax.to( content, 1, { autoAlpha:1 } );
				//TweenMax.to( container, 0.2, { autoAlpha:0 } );
				
				
				switch ( true ){
					case permalink:
						_isotope = false;
						photosetInit();
						fitvid_init();
						$('.masthead').addClass('closed');
						$('body,html').animate({scrollTop: 0}, 300);
						doComments(container,$('.post',container).data('post-id'), window.location.href.replace('#/post','post') );

						//TweenMax.to( container, 1, { autoAlpha:1 } );
						break;
					case tagged:
						$('.masthead').addClass('closed');
						$('body,html').animate({scrollTop: 0}, 300);
						isotopeInit();
						//TweenMax.to( container, 1, { autoAlpha:1 } );
						//////console.log("getPosts() :: loadTumblr(callback) :: tagged = "+tagged);
						break;
					default:
						isotopeInit();
						//TweenMax.to( container, 1, { autoAlpha:1 } );

						$('ul.media_filter li,ul.media_filter p').css('display','inline-block');

						////console.log("getPosts() :: loadTumblr(callback) :: permalink, tagged = "+permalink, tagged);
				}
				//loading = false;
				$(document).trigger('CONTENT-LOAD-DONE');

			});

			

		});
	
	};

// Disqus
	
	function doComments(container_element,post_id,post_title,post_url){
	    var _identifier = post_id,
	    	 _url = base_URL+'/#!'+_identifier;

	    disqus_url = _url;

	    if( $('#disqus_thread',container_element).length > 0 )
	        $('#disqus_thread',container_element).remove();
	        
	    container_element.append('<div id="disqus_thread" class="on"></div>');
	    
	    DISQUS.reset({
			  reload: true,
			  config: function () {  
				this.page.identifier = _identifier;
				this.page.url = _url;
			  }
		});
		
		//console.log('DISQUS',DISQUS);
		//console.log('_identifier: '+_identifier);
		//console.log('_url: '+_url);
		//console.log('doDisqus(container_element,'+_identifier+','+_url+')',container_element);
	}
	

// FB Comments
/*
	function doComments(container_element,post_id,post_title,post_url){
	    var _identifier = post_id,
	    	 _url = base_URL+'/#/'+_identifier;
	    
	    if( $('.fb-comments',container_element).length > 0 )
	        $('.fb-comments',container_element).remove();
	        
	    container_element.append('<div class="fb-comments" data-href="'+_url+'" data-colorscheme="light" data-numposts="5" data-width="740"></div>');
	 	
	 	FB.XFBML.parse(); 

	 	TweenMax.to($('.fb-comments'),0.2,{ className: "+=on", delay:0.5 });
	}
*/
// set loader
	// check activity before setting loader
	$(document).on('MH-ANIM-DONE',function() {
		activity_complete++;
		if(activity_complete==2) {
			set_loader('mh');
			activity_complete = 0;
			loading = false;
		}
	});

	$(document).on('CONTENT-LOAD-DONE',function() {
		activity_complete++;
		if(activity_complete==2) {
			set_loader('mh');
			activity_complete = 0;
			loading = false;
		}
	});

	function set_loader(setting){
		switch ( setting ) {
			case 'content' :
				loader.removeClass('mh');
				loader.addClass('content');
				loader_text.html(loader_text.data('loading'));
				TweenMax.to(loader_text, 0.5, {css:{opacity: 0}, repeat:-1, yoyo: true, delay:0.2});
			break;
			case 'mh' :
				loader.removeClass('content');
				loader.addClass('mh');
				loader_text.html(loader_text.data('scroll'));
				TweenMax.killTweensOf(loader_text);
				TweenMax.to(loader_text, 0.5, {css:{opacity: 1}});
			break;
		}
	}

// count posts
	function countPosts(content){
		switch ( true ) {
			case content.length < 10 :
				more = false;
				loadmore_text.html(loadmore_text.data('nomoreposts'));
			break;
			default :
				more = true;
				loadmore_text.html(loadmore_text.data('moreposts'));
			break;
		}
	}

// animate TT / Media filter
	function animateTTout(LN){
		TweenMax.to( $('.media_filter'), 0.6, { className: '+=on', ease:Circ.easeOut });
		TweenMax.to( $('header h1'), 0.6, { className: '+=on', ease:Circ.easeOut });
	}

	function animateTTback(LN){
		if ( Modernizr.touch )
			return false;

		TweenMax.to( $('.media_filter'), 0.6, { className: '-=on', ease:Circ.easeOut });
		TweenMax.to( $('header h1'), 0.6, { className: '-=on', ease:Circ.easeOut });
	}



/******
Clicks
******/

// click for more 
	$('body').on(settings.eventType.click, 'p.click, .video .content, .loader', function() {
		$('body,html').animate({scrollTop: (masthead.height() - $('header').height() - topOffSet)}, 300);
	});

	if ( !Modernizr.touch ){
		$( "p.click" ).hover(
			function() {
				TweenMax.to( $( "p.click.top" ), 0.3, { className: '+=closed_left' });
			}, function() {
				TweenMax.to( $( "p.click.top" ), 0.3, { className: '-=closed_left' });
			}
		);
	}

// nav button clicks
	$('.navigation li').on(settings.eventType.click, function(e) {
		if( $(this).hasClass('tix') ){
			//console.log("get tix");
			e.stopPropagation();
			e.preventDefault();
		
			var a = document.createElement("a");
				a.href = $(this).attr('href');
				window.location.hash = '#'+a.pathname;


			$('li',$(this).parent()).removeClass('on');
			$(this).addClass('on');
			
			return false;
		}
		
		//console.log("NAV CLICKED",$(this).data('type'));
		stickies = false;
		if( $(this).hasClass('release_date') )
			return;
		
		
		var nav = $(this).closest('ul').hasClass('media_filter') ? 'filter' : 'main',
			last_page_type = nav === 'main' ? page_type : last_page_type;
		
		page_type = $(this).data('type');
		page_num = 0;
		permalink = false;
		tagged = false;
		loading = true;
		
		if ( $(this).hasClass('external') ) {
			$(this).target = "_blank";
        	window.open($(this).attr('href'));
        	return false;
		}
		
		/**
		* filter clicks to target main nav only, not filter nav
		*/
		

		if( nav !== 'filter' && !hash_action_in_progress) {
			window.location.hash = '#/'+page_type;
			return;
		}
		
		set_loader('content');

		switch ( true ){
			case Modernizr.touch:
				animate = false;
				getPosts(page_type);
				break;
			default:
				mastheadRotate(page_type, $(this).parent());
		}

		/**
		* make sure we are only clearing the list the click originated from 
		* not all ul.navigation children
		*/
		$('li',$(this).parent()).removeClass('on');
			
		
		/**
		* If changing sections, make sure filter nav is reset
		*/
		if( nav === 'main' && last_page_type != page_type ) {
			if( nav === 'main' ) {
				$('ul.navigation.media_filter li').removeClass('on');
				$('ul.navigation.media_filter li[data-type=all]').addClass('on');
			}
		}
		
		$(this).addClass('on');

	});

// capture link tag clicks
	function onListLinkClick(href) {
		page_type = "new";
		page_num = 0;
		loading = true;
		animate = false;
		
		url[page_type] = href;

		permalink = url[page_type].match('post') ? true : false;
		tagged = url[page_type].match('tagged') ? true : false;
		
		if( permalink || tagged ) { // && !hash_action_in_progress
			//console.log('onListLinkClick() :: is permalink :: '+url[page_type]);
			
			if( !tagged ) 
				$('ul.media_filter li,ul.media_filter p').css('display','none');
			$('ul.media_filter li.release_date').css('display','inline-block');
			animateTTout(934);
		} else {
			//console.log('onListLinkClick() :: not permalink :: '+url[page_type]);
			
			var a = document.createElement("a");
			a.href = href;
			window.location.hash = '#'+a.pathname;

			$('ul.media_filter li,ul.media_filter p').css('display','inline-block');
			TweenMax.to( $('ul.media_filter'), 0.6, { className: '-=on', ease:Circ.easeOut });
		}
		
		getPosts(page_type);
	}
	
// permalink / tags click
	container.on(settings.eventType.click,'.post_content .permalink, .share_container a.permalink-btn, .tags a', function(e) {
		e.stopPropagation();
		e.preventDefault();
		////console.log('container.on:click', $(this) );

		var a = document.createElement("a");
			a.href = $(this).attr('href');
			window.location.hash = '#'+a.pathname;

		return false;
	});

	masthead.on(settings.eventType.click,'.spikeBtn', function(e) {
		e.stopPropagation();
		//console.log('masthead.on:click', $(this) );
		window.location.hash = $(this).attr('href');
		
		return false;
	});
	
// click for more
	$('body').on(settings.eventType.click, 'p.click, .video .content, .loader', function() {
		$('body,html').animate({scrollTop: (masthead.height() - $('header').height() - topOffSet)}, 300);
		if (Modernizr.touch)
			animateTTout(945);
	});

	$('body').on(settings.eventType.click, '.moreposts', function() {
		switch (true) {
			case $('html').hasClass('detail'):
				break;
			case !more:
				return false;
				break;
			default:
				appendNewPosts();
		}
	});



/******
Core
******/

// scroll
	$(window).on(settings.eventType.scroll, function() {
	
		var scroll_value = $(document).scrollTop();

		if( !Modernizr.touch && !safari ) {
			// parallax
			masthead.css("transform", "translate3d(0px," + parseInt(window.scrollY / 3) + "px, 0px)");
			container_wrapper.css("transform", "translate3d(0px," + parseInt(window.scrollY / 500) + "px, 0px)");
		}
			
		switch ( true ) {
			case Modernizr.touch && scroll_value > 0:
			case !Modernizr.touch && ( scroll_value >= (masthead.height() - $('header').height() - topOffSet) && !permalink) || permalink : 
				if ( !loading )
					animateTTout(1007); 
			break;
			default:
				animateTTback(1010);
			break;
		}

		// lazy load
		if($(window).scrollTop() + $(window).height() == $(document).height()) {
	       //alert("bottom!");
	    	if (more && !loading)
	    		appendNewPosts();
		}

	});

// resize
	$(window).on('resize',function() {
		var h = $(window).height() - $('header').height();
		h = h-hOffSet;
		container.css({ 'min-height': h+"px" });
		
	});
	
// hashchange
	
	window.hash_action_in_progress = false;
	
	$(window).on('hashchange',function(e) {
		if( !url_hash_init )
			return;
			
			
		hash_action_in_progress = true;
		
		/**
		* IE9 : Needed to add extra replace
		*/
		var hash = window.location.hash.replace(/#\/?/,''),
			uri = hash.match('/') ? hash.split('/') : hash;
			
		if(hash === '')
			return false;

		//console.log('window.location.hash',window.location.hash);
		//console.log('hash',hash);
		//console.log('uri',uri);

		switch( true ) {
			case uri[0] === 'post':
			case uri[0] === 'tagged':
				var href = location.protocol+'//'+location.host+'/'+hash;
				
				onListLinkClick( href );
				//console.log("hashchange() "+uri[0]);
			break;
			default:
				var p = uri;
				if( !p || p === '' )
					p = 'home';
				//console.log('nav',$('.navigation li[data-type='+p+']'));
				$('header .navigation li[data-type='+p+']').trigger(settings.eventType.click);

				animateTTback(1063);
		}
		
		hash_action_in_progress = false;
	});

// document ready
	$(document).ready( function(){
		
		switch ( true ){
			case window.location.hash == '' && !window.location.href.match('post/') && !window.location.href.match('tagged/'):
				//console.log("A: true");
				window.location.hash = '#/home';
				url_hash_init = true;
				//$(window).trigger('hashchange');
				break;

			case window.location.href.match('post/') && !window.location.href.match('#/post/'):
				//console.log("B: true");
				var a = document.createElement("a");
					a.href = window.location.href;
					window.location.href = window.location.origin+'#'+a.pathname;

				url_hash_init = true;
				//$(window).trigger('hashchange');
				break;

			case window.location.href.match('tagged/') && !window.location.href.match('#/tagged/'):
				//console.log("B: true");
				var a = document.createElement("a");
					a.href = window.location.href;
					window.location.href = window.location.origin+'#'+a.pathname;

				url_hash_init = true;
				//$(window).trigger('hashchange');
				break;

			default:
				//console.log("DEFAULT");
				url_hash_init = true;
				$(window).trigger('hashchange');
		}
	
		
	// initial content batch
		$('.tt_lockup',masthead).addClass('off');
		$('header').addClass('off');
		$('.loader').addClass('off');

		TweenMax.set( $('h1', section_current), { className: '+=closed_right' });
		TweenMax.set( $('.content', section_current), { className: '+=closed_right' });
		
		set_loader('content');
	
		custom_scrollbar();

		//mod_features.init(".modal_features");
		

		var h = $(window).height() - $('header').height();
			h = h-hOffSet;
		container.css({ 'min-height': h+"px" });

		
		pre_load(
			function(pct) {
				TweenMax.to( $('.bar'), 0.3, {css:{ width: pct+"%" }, ease:Expo.easeOut });
			},
			function() {
				$(".modal_features").css('visibility','hidden');
				TweenMax.to( $('.pre_loader_wrapper'), 0.5, { className: '+=closed', ease:settings.easing, delay:5, onComplete:function(){
						//mod_features.reset();
						//mod_features.init(".modal_features");
						//mod_features.reset();
						console.log("Reset A")
						$(".modal_features").css('visibility','visible');
						switch ( true ){
							case Modernizr.touch:
								$('header').removeClass('off');
								$('.tt_lockup',masthead).removeClass('off');
								$('.loader').removeClass('off');
								$('h1', section_current).removeClass('closed_right');
								$('.content', section_current).removeClass('closed_right');
								break;

							default:
								mod_features.init(".modal_features");
								var tl = new TimelineLite( { onComplete: removeMpaa } );
								tl.to( $('header'), 0.5, { className: '-=off', ease:settings.easing});
								tl.to( $('.tt_lockup',masthead), 0.5, { className: '-=off', ease:settings.easing});
								tl.to( $('.mpaa-container',masthead), 0.5, { className: '-=out', ease:settings.easing});
								tl.to( $('.loader'), 0.5, { className: '-=off', ease:settings.easing}, "-=0.3");
								tl.to( $('h1', section_current), 0.5, { className: '-=closed_right', ease:settings.easing}, "-=0.3");
								tl.to( $('.content', section_current), 0.5, { className: '-=closed_right', ease:settings.easing }, "-=0.3");
								tl.to( $('#tumblr_controls'), 0.5, { className: '+=on', ease:settings.easing }, "-=0.3");

								//if ( page == "home" )
								

						}

						function removeMpaa(){
							TweenMax.to($(".mpaa-container"), 0.6, { className: '+=out', ease:settings.easing, delay: 2 });	
						}

						
						
					}
				});

				
			});

// ipad orientation
	$(window).on("orientationchange", function(e) {

		if( window.orientation === 0 || window.orientation === 180 ) {
			TweenMax.set( $('.orientation_wrapper'), { className: '-=closed' });
		} else {
			TweenMax.set( $('.orientation_wrapper'), { className: '+=closed' });
		}
		});

	});

// window load

	$(window).on("load", function() {

		if( window.orientation === 0 || window.orientation === 180 ) {
			TweenMax.set( $('.orientation_wrapper'), { className: '-=closed' });
		} else {
			TweenMax.set( $('.orientation_wrapper'), { className: '+=closed' });
		}
	});



/* end  */